package examen;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;
public class S2 extends JFrame implements ActionListener {
    JButton get;
    JTextField content;
    JTextField gimme;
    String absolutePath="C:\\Users\\Vlad Miron\\Desktop\\ISP\\30122_miron_vlad_isp_labs\\examen_isp_2021\\src\\examen\\";
    S2(){
        setTitle("TextCaller");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        launch();
        setSize(200,200);
        setVisible(true);

    }
    public void launch() {
        this.setLayout(null);
        get=new JButton("Button");
        get.setBounds(50,100, 100,20);
        add(get);
        content=new JTextField();
        content.setEditable(false);
        content.setBounds(50,130,150,150);
        add(content);
        gimme=new JTextField();
        gimme.setEditable(true);
        gimme.setBounds(170, 100 , 100,20 );
        add(gimme);
        get.addActionListener(this);
    }
    public void actionPerformed(ActionEvent e)  {
        if (e.getActionCommand().equals("Button")) {
            try {
                File f = new File(absolutePath+gimme.getText());
                Scanner sc = new Scanner(f);
                String txt ="";
                while (sc.hasNextLine()) {
                    txt = txt.concat(sc.nextLine() + '\n');
                }
                content.setText(txt);
            }
            catch (FileNotFoundException exception ) {
                throw new AssertionError("File not found");

            }

        }
    }

    public static void main(String[] args)  throws FileNotFoundException {
        new S2();
    }
}
