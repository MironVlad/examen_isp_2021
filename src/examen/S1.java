package examen;


class Base {// este superclasa pentru L
public float n;
public void f(int g){

}

 }

 class L extends Base {// mosteneste Base
    private long t;
    public void f() {

    }
    public long getT(){
        return t;
    }
 }
 class X {

    public void i(L l){
        System.out.println(l.getT());
    }
 }
  class M {

  }

  class A{
    private M m;
      public A(M m) {
          this.m=m;
      }
public void metA() {// are relatie de agregare cu M

}

  }
  class B {
    private M m;
public void metB(){

}
public B ( M m) {// are relatie de compozitie cu M
    this.m=m;
}
  }

public class S1 {
    public static void main(String[] args) {
    M m=new M();
    A a= new A(m);// agregare
    B b= new B ( new M() ); // compozitie
    }
}